#include <iostream>
#include <string>
#include <vector>

struct SomeData
{
  std::string Value;
  SomeData();
  SomeData(const std::string &s);
};

struct ExprWeight
{
  std::vector< std::string > Exp;
  std::vector< int > Weights;
};

class TreeNode
{
  public:
	TreeNode( TreeNode* Father );
	SomeData  Data;
	TreeNode* LeftNode;
	TreeNode* RightNode;
	TreeNode* Father;
};
class tree
{
  private:
	TreeNode* Current;
	TreeNode* Root;

  public:
	tree();
	bool AddLeftNode(  );
	bool AddRightNode(  );

	SomeData GetData();
	bool SetData( const SomeData &Data );

	bool GoLeft();
	bool GoRight();
	bool GoUp();

	bool DeleteTree();
	~tree();
};

std::string CalculateTree( std::vector< std::string > SomeExp );
bool ChInString( const char &c , const std::string &st );
template <class T>
bool ShowTree( tree* Tr );
tree* CreateNodes( tree *root , const ExprWeight &ExpW );