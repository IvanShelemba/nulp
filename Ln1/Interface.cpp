#include "Interface.h"
#include "OtherFunctions.h"

//==========================Interpretator=======================================
Interpretator::Interpretator()
{
  this->SecondCall = 0;
}
//------------------------------------------------------------------------------
Interpretator::Interpretator( const UnicodeString &Code )
{
  this->SecondCall = 0;
  this->Parser(Code);
}
//------------------------------------------------------------------------------
bool Interpretator::FillTables( )
{
  int i = 0;
  std::list<UnicodeString> Vtmp;
  std::list<UnicodeString> Ttmp;

  while( i < this->TokenArr.size() - 1 )
  {
	if( this->TokenArr[i].GetTType() == T_DATA_TYPE_ && this->TokenArr[i+1].GetTType() == T_VOID_ )
	{
	  if( CheckVarName( this->TokenArr[i+1].GetValue() ) )
	  {
		Vtmp.push_back( this->TokenArr[i+1].GetValue() );
	  }
	  else
	  {
		DebugOut->Print("[Error] �� ��������� ��\'� �����");
		return false;
	  }
	}
	if( this->TokenArr[i].GetValue() == "thread" && this->TokenArr[i+1].GetTType() == T_VOID_ )
	{
	  if( CheckVarName( this->TokenArr[i+1].GetValue() ) )
	  {
		Ttmp.push_back( this->TokenArr[i+1].GetValue() );
	  }
	  else
	  {
		DebugOut->Print("[Error] �� ��������� ��\'� ������");
		return false;
	  }
	}
	i++;
  }

  this->VarNames.reserve( Vtmp.size() );
  this->ThreadNames.reserve( Ttmp.size() );
  while( Vtmp.size() )
  {
	for( i = 0; i < this->VarNames.size(); i++ )
	  if( Vtmp.front() == this->VarNames[i] )
	  {
		DebugOut->Print("[Error] �������� ���������� �����");
		return false;
	  }
	this->VarNames.push_back( Vtmp.front() );
	Vtmp.pop_front();
  }
  while( Ttmp.size() )
  {
	for( i = 0; i < this->ThreadNames.size(); i++ )
	  if( Ttmp.front() == this->ThreadNames[i] )
	  {
		DebugOut->Print("[Error] �������� ���������� ������");
		return false;
	  }
	this->ThreadNames.push_back( Ttmp.front() );
	Ttmp.pop_front();
  }

  return true;
}
//------------------------------------------------------------------------------
bool Interpretator::SetTokens(UnicodeString Code)
{
  if( Code == 0 || Code == "")
  {
	DebugOut->Print("[Error] Empty code.");
	return false;
  }
  Code += " \0";
  std::list<UnicodeString> tmp;
  int i = 1;
  UnicodeString t = "";
  while( i <= Code.Length() && Code[i] != '\0' )
  {
	if( i < Code.Length() && Code[i] == '/' && Code[i+1] == '/')
	  while( i < Code.Length() && Code[i] != 13 ) i++;
	if( i < Code.Length() && Code[i] == '/' && Code[i+1] == '*')
	{
	  i+=2;
	  while( i < Code.Length() && (Code[i] != '*' && Code[i+1] != '/') )i++;
	  i+=2;
	}
	if( i > Code.Length() ) break;
	if( Code[i] != ' ' && Code[i] != '	' )
	{
	  while( int(Code[i]) == 13 ) Code.Delete(i,1);
	  if( Code[i] == '\"' )
	  {
		tmp.push_back(Code[i++]);
		if( Code[i] == '\"' )
		  tmp.push_back(Code[i++]);
		else
		{
		  t = "";
		  while( i < Code.Length() && Code[i] != '\"' ) t += Code[i++];
		  if( t != "" ) tmp.push_back(t);
		  if( Code[i] == '\"' ) tmp.push_back(Code[i++]);
		}

	  }
	  else
	  if( IsSomeSym( Code[i] ) )
		tmp.push_back(Code[i++]);
	  else
	  {
		t = "";
        while( int(Code[i]) == 10 ) i++;
		while( i <= Code.Length() && Code[i] != ' ' && Code[i] != '	' && !IsSomeSym( Code[i] ) && int(Code[i]) != 13 ) t += Code[i++];
		if( t != "" && t[1] != char(10)) tmp.push_back( t );
	  }
	}
	else i++;
  }
  this->TokenArr.reserve( tmp.size() );
  while( tmp.size() )
  {
	this->TokenArr.push_back( Token( tmp.front() , GetTType( tmp.front() ) ) );
	tmp.pop_front();
  }
  return true;
}
//------------------------------------------------------------------------------
bool Interpretator::SetLexemes()
{
  int i = 0;
  std::list<Lexeme> tmp;

  while( i < this->TokenArr.size() )
  {
	if( this->TokenArr[i].GetTType() == T_KEY_WORD_ ) tmp.push_back( Lexeme( this->TokenArr[i].GetValue() , KEY_WORD ) );
	if( this->TokenArr[i].GetTType() == T_DATA_TYPE_) tmp.push_back( Lexeme( this->TokenArr[i].GetValue() , DATA_TYPE ) );
	if( this->TokenArr[i].GetTType() == T_SOME_SYM_ )
	{
	  if( this->TokenArr[i].GetValue() == ";" )
	  {
		tmp.push_back( Lexeme( this->TokenArr[i++].GetValue() , SEPSYM ) );
		continue;
	  }
	  if( this->TokenArr[i].GetValue() == "\"" )
	  {
		tmp.push_back( Lexeme( this->TokenArr[i++].GetValue() , SOME_SYM ) );
		UnicodeString t = "";
		while( i < this->TokenArr.size() && this->TokenArr[i].GetValue() != "\"" ) t += this->TokenArr[i++].GetValue();
		if( t != "" ) tmp.push_back( Lexeme( t , VALUE ) );
		tmp.push_back( Lexeme( this->TokenArr[i++].GetValue() , SOME_SYM ) );
		continue;
	  }

	  if( i < this->TokenArr.size() - 1 && this->TokenArr[i+1].GetTType() == T_SOME_SYM_ && IsSomeOp(this->TokenArr[i].GetValue() + this->TokenArr[i+1].GetValue()) )
		{
		  tmp.push_back( Lexeme( this->TokenArr[i].GetValue() + this->TokenArr[i+1].GetValue() , SOME_OP ) );
		  i+=2;
		  continue;
		}
	  else
		if( IsSomeOp(this->TokenArr[i].GetValue()) )
		{
		  tmp.push_back( Lexeme( this->TokenArr[i++].GetValue() , SOME_OP ) );
		  continue;
		}

	  tmp.push_back( Lexeme( this->TokenArr[i].GetValue() , SOME_SYM ) );
	}
	if( this->TokenArr[i].GetTType() == T_VOID_ )
	{
	  bool b = false;
	  if( this->TokenArr[i-1].GetValue() == "\"" && this->TokenArr[i+1].GetValue() == "\"" )
	  {
		tmp.push_back( Lexeme( this->TokenArr[i].GetValue() , VALUE ) );
		i++; continue;
	  }
	  for( int j = 0; j < this->VarNames.size(); j++ )
	  {
		if( this->TokenArr[i].GetValue() == this->VarNames[j] )
		{
		  tmp.push_back( Lexeme( this->TokenArr[i].GetValue() , VARIABLE ) );
		  b = true;
		  break;
		}
	  }
	  if( b ){ i++; continue; }
	  for( int j = 0; j < this->ThreadNames.size(); j++ )
	  {
		if( this->TokenArr[i].GetValue() == this->ThreadNames[j] )
		{
		  tmp.push_back( Lexeme( this->TokenArr[i].GetValue() , THREAD_IDENT ) );
		  b = true;
		  break;
		}
	  }
	  if( !b )
		tmp.push_back( Lexeme( this->TokenArr[i].GetValue() , VALUE ) );
	}
	i++;
  }
  this->LexemeArr.reserve(tmp.size());
  while( tmp.size() )
  {
	this->LexemeArr.push_back( tmp.front() );
	tmp.pop_front();
  }
  return true;
}
//------------------------------------------------------------------------------
bool Interpretator::Parser( const UnicodeString &Code )
{
  if( this->SecondCall ) this->ClrMem();
  if( !SetTokens(Code) )
  {
	DebugOut->Print("[Error] ������� ��������� �������");
	return false;
  }
  else DebugOut->Print("������� �� ������",2);
  if( !FillTables( ) )
  {
	DebugOut->Print("[Error] ������� ���������� ������� ������. ( �������� ������������ ���������� ������ �� ������� �� ���������� ���� )");
	return false;
  }
  else DebugOut->Print("������� ���������",2);
  if( !SetLexemes() )
  {
	DebugOut->Print("[Error] ������� ��������� ������");
	return false;
  }
  else DebugOut->Print("������� ��������",2);

  this->SecondCall = true;

  return true;
}

bool Interpretator::Start()
{
  id = 0;
  if( this->LexemeArr[0].GetValue() != "begin" || this->LexemeArr[this->LexemeArr.size()-1].GetValue() != "end" )
  {
	DebugOut->Print("�� �������� ����� ������� ��� ��������� ��������� ��������!");
	return false;
  }
  std::vector< SomeVar > Variables;
  Variables.reserve( this->VarNames.size() );
  std::vector< SomeThread > Threads;
  Threads.reserve( this->ThreadNames.size() );
  if(!this->Exec( this->LexemeArr , Variables , Threads ))
    return false;
  //================================================
  for( int i = 0; i < Variables.size(); i++ )
  {
	ConsoleOut->Print(Variables[i].GetType() + " " + Variables[i].GetName() + " = " + Variables[i].GetValue() + ";");
  }
  for( int i = 0; i < Threads.size(); i++ )
  {
	ConsoleOut->Print(Threads[i].GetName());
  }
  //================================================
  return true;
}
//------------------------------------------------------------------------------
bool Interpretator::Exec( std::vector<Lexeme> &LeArr , std::vector< SomeVar > &Variables , std::vector< SomeThread > &Threads )
{
  std::list< Lexeme > SomeExp;
  try
  {
	for( int i = 0; i < LeArr.size(); i++ )
	{
	  if( LeArr[i].GetLType() == L_VOID_ )
	  {
		DebugOut->Print("�������� ������� �����!");
		return false;
	  }
	  if( LeArr[i].GetLType() == KEY_WORD )
	  {
		if( LeArr[i].GetValue() == "thread")
		{
		  i++;
		  if( LeArr[i].GetLType() == THREAD_IDENT )
		  {
			Threads.push_back( SomeThread( LeArr[i].GetValue() ) );
			i++;
			if( LeArr[i].GetLType() != SEPSYM )
			{
			  if( LeArr[i].GetValue() != "<" && LeArr[i+1].GetValue() != "<" )
			  {
				DebugOut->Print("���������� ���� �������� � ����: \'<<\'. ��������: \'" + LeArr[i].GetValue() +"\'");
				return false;
			  }
			  i+=2;
			  if( LeArr[i].GetValue() != "find")
			  {
				DebugOut->Print("����������� ����� \'find\'. ��������: \'" + LeArr[i].GetValue() +"\'");
				return false;
			  }
			  SomeExp.clear();
			  bool f = true;
			  while( i < LeArr.size() && LeArr[i].GetLType() != SEPSYM )
			  {
				if( LeArr[i].GetValue() != "in" ) f = false;
				SomeExp.push_back( LeArr[i++] );
			  }
			  if(f)
			  {
				DebugOut->Print("����������� ����� \'in\'. ��������: \'" + LeArr[i-1].GetValue() +"\'");
				return false;
			  }
			  if( !Threads[Threads.size()-1].Start( SomeExp , Variables ) )
			  {
				DebugOut->Print("[Error] ������� ������");
				return false;
			  }
			  else
			  {
				ConsoleOut->Print("���� �������� �����");
			  }
			}
		  }
		  else
		  {
			DebugOut->Print("��������a�� ����� ������. ��������: \'" + LeArr[i].GetValue() +"\'");
			return false;
		  }
		}
		if( LeArr[i].GetValue() == "while" )
		{
          SomeExp.clear();
		  int KD = 0; bool fst = true;
		  while( i < LeArr.size() - 1 && (KD || fst) )
		  {
			if( LeArr[i].GetValue() == "{" ){ KD++; fst = false;}
			if( LeArr[i].GetValue() == "}" ){ KD--; fst = false;}
			SomeExp.push_back( LeArr[i++] );
		  }
		  if( !While( SomeExp , Variables , Threads ) )
		  {
			DebugOut->Print("������� ��������� while");
			return false;
		  }
		}
		if( LeArr[i].GetValue() == "if" )
		{
		  SomeExp.clear();
		  int KD = 0; bool fst = true;
		  while( i < LeArr.size() - 1 && (KD || fst) )
		  {
			if( LeArr[i].GetValue() == "{" ){ KD++; fst = false;}
			if( LeArr[i].GetValue() == "}" ){ KD--; fst = false;}
			SomeExp.push_back( LeArr[i++] );
		  }
		  KD = 0; fst = true;
		  if( LeArr[i].GetValue() == "else" )
		  {
			while( i < LeArr.size() - 1 && (KD || fst) )
			{
			  if( LeArr[i].GetValue() == "{" ){ KD++; fst = false;}
			  if( LeArr[i].GetValue() == "}" ){ KD--; fst = false;}
			  SomeExp.push_back( LeArr[i++] );
			}
		  }
		  if( !If( SomeExp , Variables , Threads ) )
		  {
			DebugOut->Print("������� ��������� if");
			return false;
		  }
		}
		if( LeArr[i].GetValue() == "find" )
		{
		  SomeExp.clear();
		  while( i < LeArr.size() - 1 && LeArr[i].GetLType() != SEPSYM )
		  {
			SomeExp.push_back( LeArr[i++] );
		  }
		  if( Find( SomeExp , Variables ) )
		  {
			ConsoleOut->Print("��������� Find");
		  }
		  else
		  {
			DebugOut->Print("������� ��������� Find");
			return false;
		  }
		}
		if( LeArr[i].GetValue() == "print" )
		{
		  SomeExp.clear();
		  while( i < LeArr.size() - 1 && LeArr[i].GetLType() != SEPSYM )
		  {
			SomeExp.push_back( LeArr[i++] );
		  }
		  if( PrintSmth( SomeExp , Variables ) )
		  {
			ConsoleOut->Print("��������� Print");
		  }
		  else
		  {
			DebugOut->Print("������� ��������� Print");
			return false;
		  }
		}
	  }
	  //------------------------------------------------------------------------//���������� ������
	  if( LeArr[i].GetLType() == DATA_TYPE )
	  {
		if( LeArr[i+1].GetLType() == VARIABLE )
		{
		  if( LeArr[i+2].GetLType() == SEPSYM )
		  {
			if( LeArr[i-1].GetValue() == "const" )
			{
			  DebugOut->Print("[WARNING] �� ��������� ��������� ��� �� ������ ��������, ���� ������ ���� �����");
			  bool fst = true;
			  for(int k = 0; k < Variables.size(); k++ )
			  {
				if( Variables[k].GetName() == LeArr[i+1].GetValue() ) fst = false;
			  }
			  if( fst )
				Variables.push_back( SomeVar( LeArr[i].GetValue() , LeArr[i+1].GetValue() , "" , false ) );
			}
			else
			{
			  bool fst = true;
			  for(int k = 0; k < Variables.size(); k++ )
			  {
				if( Variables[k].GetName() == LeArr[i+1].GetValue() ) fst = false;
			  }
			  if( fst )
				Variables.push_back( SomeVar( LeArr[i].GetValue() , LeArr[i+1].GetValue() , "" , true ) );
			}
			i += 2;
		  }
		  else
			if( LeArr[i+2].GetValue() == "=" )
			{
			  SomeExp.clear();
			  int j = i + 3;
			  if( LeArr[j].GetLType() == SEPSYM )
			  {
				DebugOut->Print("����������� ������� ������. ��������: \'" + LeArr[j].GetValue() +"\'");
				return false;
			  }
			  while( j < LeArr.size() - 1 && LeArr[j].GetLType() != SEPSYM )
				SomeExp.push_back( LeArr[j++] );
              bool fst = true;
			  for(int k = 0; k < Variables.size(); k++ )
			  {
				if( Variables[k].GetName() == LeArr[i+1].GetValue() )
				{
				  fst = false;
				  Variables[k].ChangeValue( Calculate( LeArr[i].GetValue() , SomeExp , Variables ) );
				}
			  }
			  if( fst )
			  Variables.push_back(
				SomeVar( LeArr[i].GetValue() ,
				LeArr[i+1].GetValue() ,
				Calculate( LeArr[i].GetValue() , SomeExp , Variables ) ,
				LeArr[i-1].GetValue() == "const" ? false : true )
			  );
			  i = j;
			}
			else
			{
			  DebugOut->Print("����������� \';\'. ��������: \'" + LeArr[i+1].GetValue() +"\'");
			  return false;
			}
		}
		else
		{
		  DebugOut->Print("����������� ��\'� �����. ��������: \'" + LeArr[i+1].GetValue() +"\'");
		  return false;
		}
	  }
	  //------------------------------------------------------------------------//��������� ������ �������
	  if( LeArr[i].GetLType() == VARIABLE )
	  {
	  if( LeArr[i+1].GetValue() == "=" )
	  {
        SomeExp.clear();
		int j = i + 2;
		if( LeArr[j].GetLType() == SEPSYM )
		{
		  DebugOut->Print("����������� ������� ������. ��������: \'" + LeArr[j].GetValue() +"\'");
		  return false;
		}
		while( j < LeArr.size() - 1 && LeArr[j].GetLType() != SEPSYM ) SomeExp.push_back( LeArr[j++] );
		for( int k = 0; k < Variables.size(); k++ )
		{
		  if( Variables[k].GetName() == LeArr[i].GetValue() )
			if( Variables[k].ChangeValue( Calculate( Variables[k].GetType() , SomeExp , Variables ) ) )
			  break;
			else
			{
			  DebugOut->Print("[Error!] ������� �������� �������� � �����!");
			  return false;
			}
		}
		i = j;
	  }
	  else
	  {
		DebugOut->Print("���������� ���� ���������: \'=\'. ��������: \'" + LeArr[i+1].GetValue() +"\'");
		return false;
	  }

	  }
	  //------------------------------------------------------------------------//������
	  if( LeArr[i].GetLType() == THREAD_IDENT )
	  {
		UnicodeString TID = LeArr[i].GetValue();
		i++;
		if( LeArr[i].GetValue() != "<" && LeArr[i+1].GetValue() != "<" )
		{
		  DebugOut->Print("���������� ���� �������� � ����: \'<<\'. ��������: \'" + LeArr[i].GetValue() +"\'");
		  return false;
		}
		i+=2;
		if( LeArr[i].GetValue() != "find")
		{
		  DebugOut->Print("����������� ����� \'find\'. ��������: \'" + LeArr[i].GetValue() +"\'");
		  return false;
		}
		SomeExp.clear();
		bool f = true;
		while( i < LeArr.size() && LeArr[i].GetLType() != SEPSYM)
		{
		  if( LeArr[i].GetValue() != "in" ) f = false;
		  SomeExp.push_back( LeArr[i++] );
		}
		if(f)
		{
		  DebugOut->Print("����������� ����� \'in\'. ��������: \'" + LeArr[i-1].GetValue() +"\'");
		  return false;
		}
		f = true;
		for( int j = 0; j < Threads.size(); j++ )
		{
		  if( Threads[j].GetName() == TID )
		  {
			if( !Threads[j].Start( SomeExp , Variables ) )
			{
			  DebugOut->Print("[Error] ������� ������");
			  return false;
			}
			f = false;
		  }
		}
		if(f)
		{
		  DebugOut->Print("[Error] ���� �� ���������");
		  return false;
		}
		ConsoleOut->Print("���� �������� �����");
	  }
	}
  }
  catch( int &ErrCode )
  {
	switch( ErrCode )
	{
	  case 1 : DebugOut->Print("������� ���������!"); return false;
	  case 2 : DebugOut->Print("���� ������� �����"); return false;
	  case 3 : DebugOut->Print("������� ������"); return false;
	  case 4 : DebugOut->Print(""); return false;
	  case 5 : DebugOut->Print(""); return false;
	  case 10 : DebugOut->Print("�� ������� ������� �����"); return false;
	}
	return false;
  }

}

//------------------------------------------------------------------------------

bool Interpretator::PrintSmth( std::list<Lexeme> &SomeExp ,  std::vector< SomeVar > &Variables )
{
  UnicodeString tmp = "";
  SomeExp.pop_front();
  while( SomeExp.size() )
  {
	if( SomeExp.front().GetValue() == "\"" )
	{
	  SomeExp.pop_front();
	  while( SomeExp.size() && SomeExp.front().GetValue() != "\"" )
	  {
		tmp += SomeExp.front().GetValue();
		SomeExp.pop_front();
	  }
	  SomeExp.pop_front();
	}
	else
	if( SomeExp.front().GetLType() == VARIABLE )
	{
	  bool f = true;
	  for( int i = 0; i < Variables.size(); i++ )
	  {
		if( Variables[i].GetName() == SomeExp.front().GetValue() )
		{
		  tmp += Variables[i].GetValue();
		  f = false;
		  SomeExp.pop_front();
		  break;
		}
	  }
	  if( f ) throw 2;
	}
	else
	throw 3;
  }
  DefaultOut->Print( tmp );
  return true;
}
//------------------------------------------------------------------------------
bool Interpretator::While( std::list<Lexeme> &SomeExp , std::vector< SomeVar > &Variables , std::vector< SomeThread > &Threads )
{
  SomeExp.pop_front();
  std::list<Lexeme> CalcExp;
  std::list<Lexeme> C1;
  std::vector<Lexeme> LeArr;
  if( SomeExp.front().GetValue() == "(" )
  {
	int KD = 1;
	CalcExp.clear();
	CalcExp.push_back( SomeExp.front() );
	SomeExp.pop_front();
	if( SomeExp.front().GetValue() == ")" )
	{
	  DebugOut->Print("[Error!] ����� �����!");
	  return false;
	}
	while( KD && SomeExp.size() )
	{
	  CalcExp.push_back( SomeExp.front() );
	  if( SomeExp.front().GetValue() == "("  )  KD++;
	  if( SomeExp.front().GetValue() == ")"  )  KD--;
	  SomeExp.pop_front();
	}
	LeArr.reserve(SomeExp.size() - CalcExp.size());

	C1 = CalcExp;
	if( Calculate("bool" , C1 , Variables) == "true" )
	{
	  ConsoleOut->Print( "����� �������, ��������� while" );
	  if( SomeExp.front().GetValue() == "{" )
		SomeExp.pop_front();
	  else
	  {
		DebugOut->Print("����������� \'{\'. ��������: \'" + SomeExp.front().GetValue() +"\'");
		return false;
	  }
	  if( SomeExp.front().GetValue() == "}" )
	  {
		DebugOut->Print("[Error] ������ ����");
		return false;
	  }
	  LeArr.clear();
	  while(SomeExp.size()-1)
	  {
		LeArr.push_back( SomeExp.front() );
		SomeExp.pop_front();
	  }
	  if( SomeExp.front().GetValue() != "}" )
	  {
		DebugOut->Print("����������� \'}\'. ��������: \'" + SomeExp.front().GetValue() +"\'");
		return false;
	  }
	  C1 = CalcExp;
	  while( Calculate("bool" , C1 , Variables) == "true" )
	  {
		C1 = CalcExp;
		if( !this->Exec( LeArr , Variables , Threads ))
		{
		  return false;
		}
	  }
	}
	else
	{
	  ConsoleOut->Print( "����� �����, ���� �� ����������!" );
	  return true;
	}
  }
  else
  {
	DebugOut->Print("����������� \'(\'. ��������: \'" + SomeExp.front().GetValue() +"\'");
	return false;
  }
  return true;
}
//------------------------------------------------------------------------------
bool Interpretator::If( std::list<Lexeme> &SomeExp , std::vector< SomeVar > &Variables , std::vector< SomeThread > &Threads )
{
  SomeExp.pop_front();
  std::list<Lexeme> CalcExp;
  std::vector<Lexeme> LeArr;
  if( SomeExp.front().GetValue() == "(" )
  {
	int KD = 1;
	CalcExp.clear();
	CalcExp.push_back( SomeExp.front() );
	SomeExp.pop_front();
	if( SomeExp.front().GetValue() == ")" )
	{
	  DebugOut->Print("[Error!] ����� �����!");
	  return false;
	}
	while( KD && SomeExp.size() )
	{
	  CalcExp.push_back( SomeExp.front() );
	  if( SomeExp.front().GetValue() == "("  )  KD++;
	  if( SomeExp.front().GetValue() == ")"  )  KD--;
	  SomeExp.pop_front();
	}
	if( SomeExp.front().GetValue() != "{" )
	{
	  DebugOut->Print("����������� \'{\'. ��������: \'" + SomeExp.front().GetValue() +"\'");
	  return false;
	}
	LeArr.reserve(SomeExp.size() - CalcExp.size());
	SomeExp.pop_front();
	if( SomeExp.front().GetValue() == "}" )
	{
	  DebugOut->Print("[Warning] ������ if");
	  return true;
	}
	if( Calculate("bool" , CalcExp , Variables) == "true" )
	{
	  ConsoleOut->Print( "����� �������, ��������� if" );
	  LeArr.clear();
	  while(SomeExp.size() && SomeExp.front().GetValue() != "else")
	  {
		LeArr.push_back( SomeExp.front() );
		SomeExp.pop_front();
	  }
	  if( !this->Exec( LeArr , Variables , Threads ))
	  {
		return false;
	  }
	}
	else
	{
	  ConsoleOut->Print( "����� �����, ��������� else, ���� �� �." );
	  while( SomeExp.size() && SomeExp.front().GetValue() != "else" )
		SomeExp.pop_front();
      if( !SomeExp.size() )
	  {
		return true;
	  }
	  SomeExp.pop_front();
	  if( SomeExp.front().GetValue() != "{" )
	  {
		DebugOut->Print("����������� \'{\'. ��������: \'" + SomeExp.front().GetValue() +"\'");
		return false;
	  }
	  KD = 1;
	  LeArr.clear();
	  SomeExp.pop_front();
	  if( SomeExp.front().GetValue() == "}" )
	  {
		DebugOut->Print("[Warning] ������ if");
		return true;
	  }
	  LeArr.reserve(SomeExp.size() - 1);
	  while( SomeExp.size() - 1 )
	  {
		LeArr.push_back( SomeExp.front() );
		SomeExp.pop_front();
	  }
	  if( !this->Exec( LeArr , Variables , Threads ))
	  {
		return false;
	  }
	}
  }
  else
  {
	DebugOut->Print("����������� \'(\'. ��������: \'" + SomeExp.front().GetValue() +"\'");
	return false;
  }
  return true;
}
//------------------------------------------------------------------------------
UnicodeString Interpretator::ShowTokens()
{
  if( this->TokenArr.size() == 0 ) return "";

  UnicodeString tmp = "";
  for( int i = 0; i < this->TokenArr.size(); i++ )
  {
	tmp += "\"" + this->TokenArr[i].GetValue() + "\" [" + TTypeToString( this->TokenArr[i].GetTType() ) + "]\n";
  }

  return tmp;
}
//------------------------------------------------------------------------------
UnicodeString Interpretator::ShowLexemes()
{
  if( this->LexemeArr.size() == 0 ) return "";

  UnicodeString tmp = "";
  for( int i = 0; i < this->LexemeArr.size(); i++ )
  {
	tmp += "\"" + this->LexemeArr[i].GetValue() + "\" [" + LTypeToString( this->LexemeArr[i].GetLType() ) + "]\n";
  }
  return tmp;
}
//------------------------------------------------------------------------------
bool Interpretator::ClrMem()
{
  this->TokenArr.clear();
  this->LexemeArr.clear();
  this->SecondCall = 0;
  return true;
}

//=======================Standart Out===========================================
bool StandartOut::SetOutput( TMemo* Otput )
{
  if( Otput == 0 ) return false;
  this->Out = Otput;
  return true;
}
//------------------------------------------------------------------------------
bool StandartOut::Print( const UnicodeString &Message )
{
  if( Message == 0 || Message == "" || Out == 0) return false;
  if( this->Muted ) return true;
  OldData = Message;
  Out->Lines->Add( Arrow + Message );
  return true;
}
//------------------------------------------------------------------------------
bool StandartOut::Print( const UnicodeString &Message , const unsigned int &l)
{
  if( Message == "" || Message == 0 || this->Out == 0 ) return false;
  if( this->Muted ) return true;
  OldData = Message;
  if( l > 0 )
  {
	UnicodeString tmp;
	tmp = "";
	for( int i = 0; i < l; i++ )
	  tmp += " ";
	this->Out->Lines->Add( Arrow + tmp + Message );
  }
  else
  {
	this->Out->Lines->Add( Arrow + Message );
  }

  return true;
}
//------------------------------------------------------------------------------
bool StandartOut::ShowOld()
{
  if( this->OldData == "" ) return false;
  Out->Lines->Add( Arrow + OldData );
  return true;
}
//------------------------------------------------------------------------------
bool StandartOut::GotoXY( const int &x , const int &y)
{
  if( this->Out )
  {
	this->Out->CaretPos = TPoint( 10 , 10 );
	return true;
  }
  return false;
}
//------------------------------------------------------------------------------
bool StandartOut::SetArrow( const UnicodeString &Arrow )
{
  this->Arrow = Arrow;
  return true;
}
bool StandartOut::Mute()
{
  this->Muted = true;
  return true;
}

bool StandartOut::UnMute()
{
  this->Muted = false;
  return true;
}
