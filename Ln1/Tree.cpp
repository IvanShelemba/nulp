SomeData::SomeData()
{
  Value = "";
}
SomeData::SomeData(const std::string &s)
{
  Value = s;
}
//------------------------------------------------------------------------------
tree::tree()
{
  this->Root = new TreeNode(0);
  this->Current = this->Root;
}

bool tree::AddLeftNode(  )
{
  if( this->Current->LeftNode ) return false;
  this->Current->LeftNode = new TreeNode( this->Current );
  return true;
}

bool tree::AddRightNode(  )
{
  if( this->Current->RightNode ) return false;
  this->Current->RightNode = new TreeNode( this->Current );
  return true;
}

SomeData tree::GetData()
{
  return this->Current->Data;
}

bool tree::SetData( const SomeData &Data )
{
  this->Current->Data = Data;
  return true;
}

bool tree::GoLeft()
{
  if( this->Current->LeftNode )
  {
	this->Current = this->Current->LeftNode;
	return true;
  }
  return false;
}

bool tree::GoRight()
{
  if( this->Current->RightNode )
  {
	this->Current = this->Current->RightNode;
	return true;
  }
  return false;
}

bool tree::GoUp()
{
  if( this->Current != this->Root )
  {
	this->Current = this->Current->Father;
	return true;
  }
  return false;
}

bool tree::DeleteTree()
{

}

tree::~tree()
{

}
//------------------------------------------------------------------------------

TreeNode::TreeNode( TreeNode* Father )
{
  this->Father = Father;
}
//------------------------------------------------------------------------------
std::string CalculateTree( std::vector<std::string> SomeExp )
{
  int i = 0;
  int KD = 0;
  int MaxPr = 2;

  if( SomeExp[0] == "-" ) SomeExp.insert( SomeExp.begin() , std::string("0") );
  for( i = 0; i < SomeExp.size() - 1; i++ )
  {
    if( SomeExp[i] == "(" ) KD++;
	if( SomeExp[i] == ")" ) KD--;
  }
  if( SomeExp[i] == ")" ) KD--;
  if( KD != 0 ) return "";

  ExprWeight ExpW;

  for( i = 0; i < SomeExp.size(); i++ )
  {
	if( SomeExp[i] == "(" )
	{
	  KD++;
	  continue;
	}
	if( SomeExp[i] == ")" )
	{
	  KD--;
	  continue;
	}
	if( i >= 1 && SomeExp[i] == "-" && ChInString( SomeExp[i-1][0] , "+-*/^" ) )
	{
	  ExpW.Weights.push_back( (KD + 1) * MaxPr + 0 );
	  ExpW.Exp.push_back( SomeExp[i] );
	  continue;
	}
	if( SomeExp[i] == "+" ) ExpW.Weights.push_back( KD * MaxPr + 0 );
	if( SomeExp[i] == "-" ) ExpW.Weights.push_back( KD * MaxPr + 0 );
	if( SomeExp[i] == "*" ) ExpW.Weights.push_back( KD * MaxPr + 1 );
	if( SomeExp[i] == "/" ) ExpW.Weights.push_back( KD * MaxPr + 1 );
	if( SomeExp[i] == "^" ) ExpW.Weights.push_back( KD * MaxPr + 2 );
	ExpW.Exp.push_back( SomeExp[i] );
  }

  for( i = 0; i < ExpW.Exp.size(); i++ ) std::cout<<ExpW.Exp[i]<<" ";
  std::cout<<"\n";
  for( i = 0; i < ExpW.Weights.size(); i++ ) std::cout<<ExpW.Weights[i]<<" ";

  tree *SomeTree;
  SomeTree = CreateNodes( new tree , ExpW );
  ShowTree( SomeTree );
  return "";
}
//------------------------------------------------------------------------------
bool ChInString( const char &c , const std::string &st )
{
  for( int i = 0; i < st.size(); i++ )
	if( c == st[i] ) return true;
  return false;
}
//------------------------------------------------------------------------------
tree* CreateNodes( tree *tr , const ExprWeight &ExpW )                           //fix it
{
  tr->SetData( SomeData("+") );
  tr->AddLeftNode();
  tr->GoLeft();
  tr->SetData( SomeData("2") );
  tr->GoUp();
  tr->AddRightNode();
  tr->GoRight();
  tr->SetData( SomeData("2") );
  return tr;
}
//------------------------------------------------------------------------------
bool ShowTree( tree* Tr )
{
  while(Tr->GoUp());
  Tr->GoLeft();
  std::cout<< Tr->GetData().Value ;
  Tr->GoUp();
  std::cout<< Tr->GetData().Value ;
  Tr->GoRight();
  std::cout<< Tr->GetData().Value ;
  return true;
}